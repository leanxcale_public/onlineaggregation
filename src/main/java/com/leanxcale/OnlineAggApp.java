package com.leanxcale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineAggApp {

    public static void main(String[] args) {
        SpringApplication.run(OnlineAggApp.class, args);
    }

}
