package com.leanxcale.controller;

import com.leanxcale.exception.LeanxcaleException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.sql.SQLException;

public interface Controller {

    @GetMapping("/run")
    public void runDemo() throws Exception;

    @GetMapping("/clean")
    public void cleanDemo() throws Exception;

    @GetMapping("/query")
    public void query() throws Exception;

}
