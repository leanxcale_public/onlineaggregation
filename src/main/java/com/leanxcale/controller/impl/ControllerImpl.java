package com.leanxcale.controller.impl;

import com.leanxcale.constants.Constants;
import com.leanxcale.controller.Controller;
import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.service.FunctionalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/OnlineAgg")
public class ControllerImpl implements Controller {

    private Map<String, Map<String,String>> rootProperties = null;

    @Autowired
    private FunctionalService service;

    @Override
    public void runDemo() throws Exception {
        if (rootProperties == null) {
            rootProperties = loadRootProperties("onlineagg.properties");
        }
        for (Map.Entry<String, Map<String,String>> entry : rootProperties.entrySet()) {
            service.createStructure(entry.getKey(), entry.getValue().get("url"), entry.getValue().get("database"),
                    entry.getValue().get("user"), entry.getValue().get("password"));
        }
        for (Map.Entry<String, Map<String,String>> entry : rootProperties.entrySet()) {
            System.out.println("Calling run demo for "+entry.getKey());
            Thread t = new Thread(() -> {
                try {
                    service.runDemo(entry.getKey(), entry.getValue().get("url"), entry.getValue().get("database"),
                            entry.getValue().get("user"), entry.getValue().get("password"), entry.getValue().get("dataset"));
                }
                catch (Throwable e) {
                    e.printStackTrace();
                }
            });
            t.start();
            System.out.println("Thread "+t.getName()+" created for "+entry.getKey());
        }
    }

    @Override
    public void cleanDemo() throws Exception {
        if (rootProperties == null) {
            rootProperties = loadRootProperties("onlineagg.properties");
        }
        for (Map.Entry<String, Map<String,String>> entry : rootProperties.entrySet()) {
            service.cleanDemo(entry.getKey(), entry.getValue().get("url"), entry.getValue().get("database"),
                    entry.getValue().get("user"), entry.getValue().get("password"));
        }
    }

    @Override
    public void query() throws Exception {
        if (rootProperties == null) {
            rootProperties = loadRootProperties("onlineagg.properties");
        }
        for (Map.Entry<String, Map<String,String>> entry : rootProperties.entrySet()) {
            service.query(entry.getKey(), entry.getValue().get("url"), entry.getValue().get("database"),
                    entry.getValue().get("user"), entry.getValue().get("password"));
        }
    }

    private Map<String, Map<String, String>> loadRootProperties(String name) throws IOException {
        Properties prop = new Properties();
        // Try to find external properties
        String currentDir = System.getProperty("user.dir");
        InputStream in;
        try {
            in = new FileInputStream(currentDir + "/" + name);
        }
        catch (FileNotFoundException e) {
            // External properties not found. Get the internal one
            in = Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
        }
        prop.load(in);
        Map<String, Map<String,String>> rootProperties = new HashMap<String, Map<String,String>>();
        Enumeration<String> enums = (Enumeration<String>) prop.propertyNames();
        while (enums.hasMoreElements()) {
            String key = enums.nextElement();
            String value = prop.getProperty(key);
            String[] parts = key.split("\\.");
            if (rootProperties.get(parts[0]) == null) {
                rootProperties.put(parts[0],new HashMap<String, String>());
            }
            rootProperties.get(parts[0]).put(parts[1], value);
        }
        return rootProperties;
    }

}
