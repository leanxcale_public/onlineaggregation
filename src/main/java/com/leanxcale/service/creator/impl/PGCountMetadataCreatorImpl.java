package com.leanxcale.service.creator.impl;

import com.leanxcale.constants.Constants;
import com.leanxcale.service.creator.MetadataCreator;
import java.sql.*;

public class PGCountMetadataCreatorImpl implements MetadataCreator {

    @Override
    public void dropTables(String url, String database, String user, String password) throws Exception {

        Statement stmt = null;

        if (tableExists(url, database, user, password, Constants.TABLE_NAME.toLowerCase())) {
            try (Connection conn = DriverManager.getConnection(url+"/"+database, user, password)) {
                stmt = conn.createStatement();
                String sql = "DROP TABLE " + Constants.TABLE_NAME.toLowerCase() ;
                stmt.executeUpdate(sql);
                stmt.close();
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        }
        if (tableExists(url, database, user, password, Constants.ID_COUNT_PG_TABLE_NAME.toLowerCase())) {
            try (Connection conn = DriverManager.getConnection(url+"/"+database, user, password)) {
                stmt = conn.createStatement();
                String sql = "DROP TABLE "+Constants.ID_COUNT_PG_TABLE_NAME.toLowerCase();
                stmt.executeUpdate(sql);
                stmt.close();
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        }
        if (tableExists(url, database, user, password, Constants.DATE_COUNT_PG_TABLE_NAME.toLowerCase())) {
            try (Connection conn = DriverManager.getConnection(url+"/"+database, user, password)) {
                stmt = conn.createStatement();
                String sql = "DROP TABLE "+Constants.DATE_COUNT_PG_TABLE_NAME.toLowerCase();
                stmt.executeUpdate(sql);
                stmt.close();
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        }
        if (triggerExists(url, database, user, password, "trigger_agg_id")) {
            try (Connection conn = DriverManager.getConnection(url+"/"+database, user, password)) {
                stmt = conn.createStatement();
                String sql = "DROP TRIGGER trigger_agg_id";
                stmt.executeUpdate(sql);
                stmt.close();
            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        }
        if (triggerExists(url, database, user, password, "trigger_agg_postdate")) {
            try (Connection conn = DriverManager.getConnection(url+"/"+database, user, password)) {
                stmt = conn.createStatement();
                String sql = "DROP TRIGGER trigger_agg_postdate";
                stmt.executeUpdate(sql);

            }
            finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        }

    }

    @Override
    public void createTables(String url, String database, String user, String password) throws Exception {

        Statement stmt = null;

        // Create table
        // info table
        if (!tableExists(url, database, user, password, Constants.TABLE_NAME.toLowerCase())) {
            try (Connection conn = DriverManager.getConnection(url+"/"+database, user, password)) {
                stmt = conn.createStatement();
                String sql = "CREATE TABLE " + Constants.TABLE_NAME.toLowerCase() +
                        "(ID            VARCHAR     NOT NULL," +
                        " POSTDATE       TIMESTAMP(3)    NOT NULL, " +
                        " OTHERFIELD     VARCHAR, " +
                        "PRIMARY KEY(ID, POSTDATE))";
                stmt.executeUpdate(sql);
            } finally {
                if (stmt != null) {
                    stmt.close();
                }
            }
        }

    }

    private boolean tableExists(String url, String database, String user, String password, String tableName) throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try (Connection conn = DriverManager.getConnection(url+"/"+database, user, password)) {

            String sql = "SELECT EXISTS (\n" +
                    "   SELECT 1\n" +
                    "   FROM   information_schema.tables \n" +
                    "   WHERE  table_schema = 'public'\n" +
                    "   AND    table_name = '"+tableName+"'\n" +
                    "   );";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            boolean res = false;
            while (rs.next()) {
                res = rs.getBoolean(1);
            }
            return res;
        }
        finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }

    private boolean triggerExists(String url, String database, String user, String password, String triggerName) throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try (Connection conn = DriverManager.getConnection(url+"/"+database, user, password)) {

            String sql = "SELECT tgname" +
                    "   from pg_trigger" +
                    "   where not tgisinternal AND tgname='"+triggerName+"'";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            String res = "";
            while (rs.next()) {
                res = rs.getString(1);
            }
            return res.equals(triggerName);
        }
        finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }
}
