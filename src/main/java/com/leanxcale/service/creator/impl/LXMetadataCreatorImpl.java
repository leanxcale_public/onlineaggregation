package com.leanxcale.service.creator.impl;

import com.leanxcale.constants.Constants;
import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.database.DeltaType;
import com.leanxcale.kivi.database.Field;
import com.leanxcale.kivi.database.Type;
import com.leanxcale.kivi.session.Credentials;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.service.creator.MetadataCreator;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class LXMetadataCreatorImpl implements MetadataCreator {


    @Override
    public void dropTables(String url, String database, String user, String password) throws IOException, LeanxcaleException {
        Settings settings = new Settings();
        settings.credentials(new Credentials().setDatabase(database).setUser(user).setPass(password.toCharArray()));
        settings.transactional();
        settings.deferSessionFactoryClose(10000);
        try (Session session = SessionFactory.newSession(url, settings)) {
            if (session.database().tableExists(Constants.TABLE_NAME)) {
                session.database().dropTable(Constants.TABLE_NAME);
            }
            if (session.database().tableExists(Constants.ID_COUNT_DELTA_TABLE_NAME)) {
                session.database().dropTable(Constants.ID_COUNT_DELTA_TABLE_NAME);
            }
            if (session.database().tableExists(Constants.DATE_COUNT_DELTA_TABLE_NAME)) {
                session.database().dropTable(Constants.DATE_COUNT_DELTA_TABLE_NAME);
            }
        }
    }

    @Override
    public void createTables(String url, String database, String user, String password) throws IOException, LeanxcaleException {
        Settings settings = new Settings();
        settings.credentials(new Credentials().setDatabase(database).setUser(user).setPass(password.toCharArray()));
        settings.transactional();
        settings.deferSessionFactoryClose(1000);

        try (Session session = SessionFactory.newSession(url, settings)) {
            // Table creation: {PK[id,postdate],otherfield}
            if (!session.database().tableExists(Constants.TABLE_NAME)) {
                List<Field> keyFields = Arrays
                        .asList(new Field[]{new Field("id", Type.STRING),
                                (new Field("postdate", Type.TIMESTAMP))});
                List<Field> fields = Arrays.asList(new Field[]{new Field("otherfield", Type.STRING)});
                session.database().createTable(Constants.TABLE_NAME, keyFields, fields);
            }
            // Aggregation table creation:{PK[id],count}
            if (!session.database().tableExists(Constants.ID_COUNT_DELTA_TABLE_NAME)) {
                List<Field> keyFields = Arrays
                        .asList(new Field[]{new Field("id", Type.STRING)});
                List<Field> fields = Arrays.asList(new Field[]{new Field("count", Type.LONG, DeltaType.ADD)});
                session.database().createTable(Constants.ID_COUNT_DELTA_TABLE_NAME, keyFields, fields);
            }
            // Aggregation table creation:{PK[postdate],count}
            if (!session.database().tableExists(Constants.DATE_COUNT_DELTA_TABLE_NAME)) {
                List<Field> keyFields = Arrays
                        .asList(new Field[]{new Field("postdate", Type.DATE)});
                List<Field> fields = Arrays.asList(new Field[]{new Field("count", Type.LONG, DeltaType.ADD)});
                session.database().createTable(Constants.DATE_COUNT_DELTA_TABLE_NAME, keyFields, fields);
            }
        }
    }
}
