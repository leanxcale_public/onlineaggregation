package com.leanxcale.service.creator;

import com.leanxcale.exception.LeanxcaleException;

import java.io.IOException;
import java.sql.SQLException;

public interface MetadataCreator {

    public void dropTables(String url, String database, String user, String password) throws Exception;

    public void createTables(String url, String database, String user, String password) throws Exception;

}
