package com.leanxcale.service.factory;

import com.leanxcale.service.creator.MetadataCreator;

import java.io.IOException;

public interface MetadataCreatorFactory {

    public MetadataCreator getCreator(String name) throws InstantiationException, IOException, IllegalAccessException, ClassNotFoundException;

}
