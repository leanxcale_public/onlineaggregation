package com.leanxcale.service.factory.impl;

import com.leanxcale.service.creator.MetadataCreator;
import com.leanxcale.service.executor.QueryExecutor;
import com.leanxcale.service.factory.QueryExecutorFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.stereotype.Service;

@Service
public class QueryExecutorFactoryImpl implements QueryExecutorFactory {

    private static ConcurrentHashMap<String, String> classes = null;

    private static final String propsName = "factory/queryExecutorFactory.properties";

    private static Object lock = new Object();


    @Override
    public QueryExecutor getExecutor(String name) throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        synchronized (lock) {
            if (classes == null) loadQueryExecutorFactoryProperties();
        }
        return (QueryExecutor)Class.forName(classes.get(name)).newInstance();
    }

    private void loadQueryExecutorFactoryProperties () throws IOException {

        Properties prop = new Properties();
        InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsName);
        prop.load(in);

        classes = new ConcurrentHashMap<String, String>();
        Enumeration<String> enums = (Enumeration<String>) prop.propertyNames();
        while (enums.hasMoreElements()) {
            String key = enums.nextElement();
            classes.put(key, prop.getProperty(key));
        }
    }

}
