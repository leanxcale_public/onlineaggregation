package com.leanxcale.service.factory.impl;

import com.leanxcale.service.factory.DataLoaderFactory;
import com.leanxcale.service.loader.DataLoader;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Service
public class DataLoaderFactoryImpl implements DataLoaderFactory {

    private static final String propsName = "factory/dataLoaderFactory.properties";

    private static ConcurrentHashMap<String, String> classes;
    private static Object lock = new Object();

    @Override
    public DataLoader getLoader(String name) throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        synchronized (lock) {
            if (classes == null) {
                loadDataLoaderFactoryProperties();
            }
        }
        System.out.println("Factory creating new instance of "+classes.get(name)+" from property "+name);
        return (DataLoader)Class.forName(classes.get(name)).newInstance();
    }

    private static void loadDataLoaderFactoryProperties () throws IOException {

        Properties prop = new Properties();
        InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsName);
        prop.load(in);

        classes = new ConcurrentHashMap<String, String>();
        Enumeration<String> enums = (Enumeration<String>) prop.propertyNames();
        while (enums.hasMoreElements()) {
            String key = enums.nextElement();
            classes.put(key, prop.getProperty(key));
        }
    }

}
