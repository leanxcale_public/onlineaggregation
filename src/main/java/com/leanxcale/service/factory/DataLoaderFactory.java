package com.leanxcale.service.factory;

import com.leanxcale.service.loader.DataLoader;
import org.springframework.stereotype.Service;

import java.io.IOException;

public interface DataLoaderFactory {

    public DataLoader getLoader(String name) throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException;

}
