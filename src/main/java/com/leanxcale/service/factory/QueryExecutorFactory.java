package com.leanxcale.service.factory;

import com.leanxcale.service.executor.QueryExecutor;
import com.leanxcale.service.loader.DataLoader;
import java.io.IOException;

public interface QueryExecutorFactory {

    public QueryExecutor getExecutor(String name) throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException;

}
