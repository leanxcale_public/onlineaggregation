package com.leanxcale.service.impl;

import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.service.FunctionalService;
import com.leanxcale.service.factory.DataLoaderFactory;
import com.leanxcale.service.factory.MetadataCreatorFactory;
import com.leanxcale.service.factory.QueryExecutorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class FunctionalServiceImpl implements FunctionalService {

    @Autowired
    private DataLoaderFactory dataLoaderFactory;

    @Autowired
    private MetadataCreatorFactory metadataCreatorFactory;

    @Autowired
    private QueryExecutorFactory queryExecutorFactory;

    @Override
    public void runDemo(String name, String url, String database, String usr, String passwd, String dataset) {
        try {
            Map<String, List<String>> rootProperties = loadCsvNames(name, dataset);
            // Get the appropriate csv list
            List<String> csvs = rootProperties.get(name);
            for (String csvName: csvs) {
                try {
                    dataLoaderFactory.getLoader(name).loadData(url, csvName, database, usr, passwd);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                System.out.println("Thread "+Thread.currentThread().getName()+" loading "+csvName+ " for "+name);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cleanDemo(String name, String url, String database, String user, String password) throws Exception {
        metadataCreatorFactory.getCreator(name).dropTables(url, database, user, password);
    }

    @Override
    public void createStructure(String name, String url, String database, String usr, String passwd) throws Exception {
        metadataCreatorFactory.getCreator(name).createTables(url, database, usr, passwd);
    }

    @Override
    public void query(String name, String url, String database, String user, String password) throws Exception {
        Thread t = new Thread(() -> {
            try {
                queryExecutorFactory.getExecutor(name).query(url, database, user, password);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        t.start();
    }

    private Map<String, List<String>> loadCsvNames(String name, String dataset) throws IOException {
        // Load properties file to get files to load
        Properties props = new Properties();
        // Try to find external file
        String currentDir = System.getProperty("user.dir");
        InputStream ins;
        try {
            ins = new FileInputStream(currentDir + "/" + dataset);
        }
        catch (FileNotFoundException e) {
            // External properties not found. Get the internal one
            ins = Thread.currentThread().getContextClassLoader().getResourceAsStream(dataset);
        }
        props.load(ins);

        Map<String, List<String>> res = new HashMap<String, List<String>>();

        String csvsString = (String) props.getProperty("csvs");
        String[] parts = csvsString.split(",");
        if(res.get(name) == null) {
            List<String> csvs = new ArrayList<String>();
            for (int i = 0; i < parts.length; i++) {
                csvs.add(parts[i]);
            }
            res.put(name, csvs);
        }
        return res;
    }
}
