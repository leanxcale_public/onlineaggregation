package com.leanxcale.service;

import com.leanxcale.exception.LeanxcaleException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

public interface FunctionalService {

    public void runDemo(String name, String url, String database, String usr, String passwd, String dataset);

    public void cleanDemo(String name, String url, String database, String usr, String passwd) throws Exception;

    public void createStructure(String name, String url, String database, String usr, String passwd) throws Exception;

    public void query(String name, String url, String database, String user, String password) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException, Exception;
}
