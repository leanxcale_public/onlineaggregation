package com.leanxcale.service.executor;

public interface QueryExecutor {

    public void query(String url, String database, String user, String password) throws Exception;

}
