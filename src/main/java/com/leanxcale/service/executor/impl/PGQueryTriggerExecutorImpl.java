package com.leanxcale.service.executor.impl;

import com.leanxcale.constants.Constants;
import com.leanxcale.service.executor.QueryExecutor;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PGQueryTriggerExecutorImpl implements QueryExecutor {

    private Connection conn;
    private PreparedStatement pstmt = null;
    private ResultSet rs = null;

    private int rows = 0;

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");

    private static final Logger log = LoggerFactory.getLogger(PGQueryTriggerExecutorImpl.class);

    @Override
    public void query(String url, String database, String user, String password) throws Exception {

        try {
            if (conn == null) {
                conn = DriverManager.getConnection(url + "/" + database, user, password);
                conn.setAutoCommit(false);
            }

            while (rows <= 15000000) {
                String sql = "SELECT COUNT(*) FROM " + Constants.TABLE_NAME.toLowerCase();
                int res = 0;
                log.info("-----------------------------------------------------------------------------------------------------------");
                log.info("PG: Querying info table for number of rows...");
                pstmt = conn.prepareStatement(sql);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    res = rs.getInt(1);
                    log.info("Rows: " + res);
                }
                rows=res;
                rs.close();
                pstmt.close();
                log.info("PG Querying info table done!");


                String sql2 = "SELECT * FROM " + Constants.ID_COUNT_PG_TABLE_NAME.toLowerCase();
                log.info("-----------------------------------------------------------------------------------------------------------");
                log.info("PG: Querying info id table...");
                long t1 = System.currentTimeMillis();
                pstmt = conn.prepareStatement(sql2);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    log.info("Id: " + rs.getString(1) + " Count: " + rs.getInt(2));
                }
                rs.close();
                pstmt.close();
                long t2 = System.currentTimeMillis();
                log.info("PG query time: {} ms", t2 - t1);
                log.info("PG: Querying info id table done!");


                String sql3 = "SELECT * FROM " + Constants.DATE_COUNT_PG_TABLE_NAME.toLowerCase();
                log.info("-----------------------------------------------------------------------------------------------------------");
                log.info("PG: Querying info date table...");
                long t3 = System.currentTimeMillis();
                pstmt = conn.prepareStatement(sql3);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    log.info("Date: " + rs.getDate(1) + " Count: " + rs.getInt(2));
                }
                rs.close();
                pstmt.close();
                long t4 = System.currentTimeMillis();
                log.info("PG query time: {} ms", t4 - t3);
                log.info("PG Querying info date table done!");

                Thread.sleep(5000);
            }
        }
        finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

}
