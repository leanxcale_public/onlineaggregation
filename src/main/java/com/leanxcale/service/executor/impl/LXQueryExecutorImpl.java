package com.leanxcale.service.executor.impl;

import com.leanxcale.constants.Constants;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.query.Aggregation;
import com.leanxcale.kivi.query.TupleIterable;
import com.leanxcale.kivi.query.aggregation.Aggregations;
import com.leanxcale.kivi.session.Credentials;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.service.executor.QueryExecutor;
import com.leanxcale.service.loader.impl.PGCSVDataLoaderImpl;
import java.util.Arrays;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

    public class LXQueryExecutorImpl implements QueryExecutor {

    private static final Logger log = LoggerFactory.getLogger(LXQueryExecutorImpl.class);

    private Session session = null;

    private Table infoTable;
    private Table infoIdTable;
    private Table infoPostdateTable;

    private int rows = 0;

    @Override
    public void query(String url, String database, String user, String password) throws Exception {

        Settings settings = new Settings();
        Credentials credentials = new Credentials();
        credentials.setDatabase(database).setUser(user);
        credentials.setDatabase(database).setPass(password.toCharArray());
        settings.credentials(credentials);
        settings.transactional();
        settings.deferSessionFactoryClose(10000);

        try{
            session= SessionFactory.newSession(url, settings);
            infoTable = session.database().getTable(Constants.TABLE_NAME);
            infoIdTable = session.database().getTable(Constants.ID_COUNT_DELTA_TABLE_NAME);
            infoPostdateTable = session.database().getTable(Constants.DATE_COUNT_DELTA_TABLE_NAME);

            while (rows <= 15000000) {
                log.info("-----------------------------------------------------------------------------------------------------------");
                log.info("LX: Querying info table for number of rows...");
                TupleIterable res = infoTable.find().aggregate(Collections.emptyList(), Aggregations.count("numRows"));
                res.forEach(tuple -> {
                    long value = tuple.getLong("numRows");
                    log.info("Rows: " + value);
                    rows = Math.toIntExact(value);
                });
                log.info("LX Querying info table done!");

                log.info("-----------------------------------------------------------------------------------------------------------");
                log.info("LX: Querying id delta table...");
                long t1 = System.currentTimeMillis();
                TupleIterable res2 = infoIdTable.find();
                res2.forEach(tuple -> log.info("Id: " + tuple.getString("id") + " Count: " + tuple.getLong("count")));
                long t2 = System.currentTimeMillis();
                log.info("LX Query time: {} ms", t2 - t1);
                log.info("LX Querying id delta table done!");

                log.info("-----------------------------------------------------------------------------------------------------------");
                log.info("LX: Querying postdate delta table...");
                long t3 = System.currentTimeMillis();
                TupleIterable res3 = infoPostdateTable.find();
                res3.forEach(tuple -> log.info("Date: " + tuple.getDate("postdate") + " Count: " + tuple.getLong("count")));
                session.commit();
                long t4 = System.currentTimeMillis();
                log.info("LX Query time: {} ms", t4 - t3);
                log.info("LX Querying id delta table done!");

                Thread.sleep(5000);
            }
        }
        finally {
            if (session == null) {
                session.close();
            }
        }

    }
}
