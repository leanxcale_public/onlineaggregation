package com.leanxcale.service.loader;

import com.leanxcale.exception.LeanxcaleException;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.concurrent.atomic.AtomicReference;

public interface DataLoader {

    public void loadData (String url , String path, String database, String user, String password) throws Exception;

}
