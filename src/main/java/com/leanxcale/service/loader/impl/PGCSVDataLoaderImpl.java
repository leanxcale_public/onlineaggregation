package com.leanxcale.service.loader.impl;

import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.service.loader.DataLoader;
import java.io.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PGCSVDataLoaderImpl implements DataLoader {

    private Connection conn;
    private PreparedStatement pstmt = null;

    private static int rowsInserted = 0;
    private static Object lock = new Object();

    private static final Logger log = LoggerFactory.getLogger(PGCSVDataLoaderImpl.class);

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");

    @Override
    public void loadData(String url, String path, String database, String user, String password) throws Exception {

        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ",";
        InputStream in = null;

        try {
            in = new FileInputStream(path);
            br = new BufferedReader(new InputStreamReader(in));

            while ((line = br.readLine()) != null) {
                String[] parts = line.split(csvSplitBy);
                insert(url, database, user, password, parts[0], parts[1], parts[2]);
            }
        }
        finally {
            if (in != null) {
                in.close();
            }
            if (br != null) {
                br.close();
            }
        }

    }

    private void insert (String url, String database, String user, String password, String id, String postdate, String otherField) throws SQLException, ParseException {
        try {
            if (conn == null) {
                conn = DriverManager.getConnection(url+"/"+database, user, password);
                conn.setAutoCommit(false);
            }

            Date date = dateFormat.parse(postdate);

            String sql = "INSERT INTO INFO (ID, POSTDATE, OTHERFIELD) VALUES (?,?,?)";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,id);
            pstmt.setTimestamp(2, new Timestamp(date.getTime()));
            pstmt.setString(3, otherField);
            long t1 = System.nanoTime();
            pstmt.execute();
            long t2 = System.nanoTime();
            synchronized (lock) {
                rowsInserted++;
            }
            if (log.isDebugEnabled()) {
                log.debug("PG inserted row: " + rowsInserted + ", insertion time: " + (t2 - t1));
            }

            pstmt.close();
            boolean doCommit = false;
            synchronized (lock) {
                if (rowsInserted % 1000 == 0) {
                    doCommit = true;
                }
            }
            if (doCommit) {
                conn.commit();
                conn.close();
                conn = null;
                doCommit = false;
                synchronized (lock) {
                    System.out.println("PG inserted: " + rowsInserted + " with thread "+ Thread.currentThread().getName());
                }
            }
        } catch (SQLException e) {
            if (conn != null) {
                conn.close();
                conn = null;
            }
            throw e;
        }
    }
}
