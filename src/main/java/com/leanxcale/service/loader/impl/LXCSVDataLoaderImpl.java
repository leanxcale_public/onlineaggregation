package com.leanxcale.service.loader.impl;

import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.session.Credentials;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.kivi.tuple.Tuple;
import com.leanxcale.service.loader.DataLoader;
import java.io.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LXCSVDataLoaderImpl implements DataLoader {

    private static final String TABLE_NAME = "INFO";
    private static final String ID_COUNT_DELTA_TABLE_NAME = "INFO_ID_DELTA";
    private static final String DATE_COUNT_DELTA_TABLE_NAME = "INFO_DATE_DELTA";

    private Session session = null;
    private Table infoTable = null;
    private Table infoIdTable = null;
    private Table infoPostdateTable = null;

    public static Object lock = new Object();

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");

    private static final Logger log = LoggerFactory.getLogger(LXCSVDataLoaderImpl.class);

    private static int rowsInserted = 0;

    @Override
    public void loadData(String url, String path, String database, String user, String password) throws Exception {

        BufferedReader br = null;
        InputStream in = null;
        String line = "";
        String csvSplitBy = ",";

        Settings settings = new Settings();
        Credentials credentials = new Credentials();
        credentials.setDatabase(database).setUser(user);
        credentials.setDatabase(database).setPass(password.toCharArray());
        settings.credentials(credentials);
        settings.transactional();
        settings.deferSessionFactoryClose(10000);

        try  {
            if (session == null) {
                session = SessionFactory.newSession(url, settings);
            }
            infoTable = session.database().getTable(TABLE_NAME);
            infoIdTable = session.database().getTable(ID_COUNT_DELTA_TABLE_NAME);
            infoPostdateTable = session.database().getTable(DATE_COUNT_DELTA_TABLE_NAME);

            in = new FileInputStream(path);
            br = new BufferedReader(new InputStreamReader(in));
            int lineNumber = 0;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split(csvSplitBy);
                Tuple tuple = null;
                // Info
                tuple = infoRow2Tuple(parts);
                long t1 = System.nanoTime();
                infoTable.insert(tuple);
                long t2 = System.nanoTime();
                long insertTime = t2 - t1;
                // Info_id
                tuple = infoIdRow2Tuple(parts);
                long t3 = System.nanoTime();
                infoIdTable.upsert(tuple);
                long t4 = System.nanoTime();
                insertTime += (t4 - t3);
                //Info_postdate
                tuple = infoPostdateRow2Tuple(parts);
                long t5 = System.nanoTime();
                infoPostdateTable.upsert(tuple);
                long t6 = System.nanoTime();
                insertTime += (t6 - t5);
                synchronized (lock) {
                    rowsInserted++;
                }
                if (log.isDebugEnabled()) {
                    log.debug("LX inserted row: " + rowsInserted + ", insertion time: " + insertTime);
                }
                if (rowsInserted % 1000 == 0) {
                    session.commit();
                    System.out.println("LX " + rowsInserted);
                }
                boolean doCommit = false;
                synchronized (lock) {
                    if (rowsInserted % 1000 == 0) {
                        doCommit = true;
                    }
                }
                if (doCommit) {
                    session.commit();
                    doCommit = false;
                    synchronized (lock) {
                        System.out.println("LX " + rowsInserted);
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (in != null) {
                in.close();
            }
            if (br != null) {
                br.close();
            }
            if (session != null) {
                session.commit();
                session.close();
            }
        }
    }

    private Tuple infoRow2Tuple(String[] parts) throws ParseException {
        Tuple tuple = infoTable.createTuple();
        tuple.putString("id", parts[0]);
        Date date = dateFormat.parse(parts[1]);
        tuple.putTimestamp("postdate", new Timestamp(date.getTime()));
        tuple.putString("otherfield", parts[2]);
        return tuple;
    }

    private Tuple infoIdRow2Tuple(String[] parts) {
        Tuple tupleIdDelta = infoIdTable.createTuple();
        tupleIdDelta.putString("id", parts[0]);
        tupleIdDelta.putLong("count", 1L);
        return tupleIdDelta;
    }

    private Tuple infoPostdateRow2Tuple(String[] parts) throws ParseException {
        Tuple tupleDateDelta = infoPostdateTable.createTuple();
        Date date = dateFormat.parse(parts[1]);
        tupleDateDelta.putDate("postdate", new java.sql.Date(date.getTime()));
        tupleDateDelta.putLong("count", 1L);
        return tupleDateDelta;
    }
}
