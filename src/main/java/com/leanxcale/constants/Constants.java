package com.leanxcale.constants;

public class Constants {

    public static final String STATUS_RUNNING = "running";
    public static final String STATUS_STOPPED = "stopped";
    public static final String STATUS_CLEAN = "clean";


    public static final String TABLE_NAME = "INFO";
    public static final String ID_COUNT_DELTA_TABLE_NAME = "INFO_ID_DELTA";
    public static final String DATE_COUNT_DELTA_TABLE_NAME = "INFO_DATE_DELTA";

    public static final String ID_COUNT_PG_TABLE_NAME = "INFO_ID";
    public static final String DATE_COUNT_PG_TABLE_NAME = "INFO_POSTDATE";

}
